//Our lovely widget
var iframe = document.getElementById('scp_iframe_general');

var currentTemplate;
var currentTheme;
var currentPosition;
var currentBG;

//Search current template in class
iframeClassArray = iframe.getAttribute("class").split(" ");
var i;
for (i = 0; i < iframeClassArray.length; i++) {
  if (iframeClassArray[i].includes("template")) {
    currentTemplate = iframeClassArray[i];
  }
  if (iframeClassArray[i].includes("pos-")) {
    currentPosition = iframeClassArray[i];
  }
  if (iframeClassArray[i].includes("wbg")) {
    currentBG = iframeClassArray[i];
  }
}
currentTemplate = currentTemplate.replace("-template", "");

//Search current theme in class
if (iframe.getAttribute("src").includes("dark")) {
  currentTheme = "dark";
} else {
  currentTheme = "light";
}


//Resizes the iframe when .embed-template is assigned, then updates property name.
function loadIframe() {
  var isEmbed = iframe.getAttribute("class").includes("embed"); //returns true (found) or false (not found)

  if (isEmbed) {
    resizeIframe(iframe);
  } else {
    iframe.removeAttribute("style"); //removes inline style with embed size
  }

  //Changes name of the property with the selected one.
  getPropertyName();
  changeColor();

}

//Updates the iframe src with the new template
function loadTemplate(template) {
  newURL = "/revyoos-widgets/revyoos/widget.html?theme=" + currentTheme + "&template=" + template;
  newClass = template + "-template pos-left-settings";
  iframe.setAttribute("src", newURL);
  iframe.setAttribute("class", newClass);

  currentTemplate = template;

  setupPage = template + "-settings.html";
  document.getElementById("btn-setup").setAttribute("href", setupPage);
}


//Changes name of the property with the selected one.
function getPropertyName() {
  propertyName = document.getElementById("selectProperty").value;
  propertyDivs = iframe.contentWindow.document.getElementsByClassName('revyoos-holding-name');

  /* Default value (demo purposes only, default value is set up as company name) */
  if (propertyName == "") {
    propertyName = "Costa CarpeDiem";
  }

  /* Changes all divs with that property name */
  var i;
  for (i = 0; i < propertyDivs.length; i++) {
    propertyDivs[i].innerHTML = propertyName;
  }
}


//Changes #main background setting a css class.
function changeBackground(bg) {
  document.getElementById('main').setAttribute("class", bg);
}


//Change theme
function changeTheme(theme) {
  newURL = "/revyoos-widgets/revyoos/widget.html?theme=" + theme + "&template=" + currentTemplate;
  newClass = currentTemplate + "-template";
  if (theme == "dark") {
    newClass += " effect-blur";
  }
  if (currentPosition) {
    newClass += " " + currentPosition;
  }
  var widgetBody = iframe.contentWindow.document.getElementById('revyoos-widget');
  widgetBody.removeAttribute("style");

  iframe.setAttribute("src", newURL);
  iframe.setAttribute("class", newClass);

  currentTheme = theme;

  if (document.querySelector('input[name="btnradio-wbg"]:checked') != null) {
    document.querySelector('input[name="btnradio-wbg"]:checked').checked = false;
  }

}

//Change position
function changePosition(position) {
  newURL = "/revyoos-widgets/revyoos/widget.html?theme=" + currentTheme + "&template=" + currentTemplate;
  newClass = currentTemplate + "-template " + position;
  if (currentTheme == "dark") {
    newClass += " effect-blur";
  }
  iframe.setAttribute("src", newURL);
  iframe.setAttribute("class", newClass);

  currentPosition = position;
}

//Change Widget Background
function changeWBG(wbg) {
  var widgetBody = iframe.contentWindow.document.getElementById('revyoos-widget');
  widgetBody.setAttribute("style", "background-color:transparent !important;");

  iframeClassArray = iframe.getAttribute("class").split(" ");
  hasWBG = iframe.getAttribute("class").includes("wbg");

  var newClass = "";
  for (i = 0; i < iframeClassArray.length; i++) {
    if (iframeClassArray[i].includes("wbg")) {
      iframeClassArray[i] = "wbg-" + wbg;
    }
    newClass += iframeClassArray[i] + " ";
  }
  if (!hasWBG) {
    newClass += "wbg-" + wbg;
  }

  iframe.setAttribute("class", newClass);

}

//Change color
function defaultColor(color) {
  document.getElementById("colorPicker").value = color;
  changeColor();
}

function changeColor() {
  if (document.getElementById("colorPicker")) {
    newColor = document.getElementById("colorPicker").value;
    var i;
    var revyoosHolding = iframe.contentWindow.document.getElementsByClassName('revyoos-holding');
    for (i = 0; i < revyoosHolding.length; i++) {
      revyoosHolding[i].style.backgroundColor = newColor;
    }
    var revyoosHoldingName = iframe.contentWindow.document.getElementsByClassName('revyoos-holding-name');
    for (i = 0; i < revyoosHoldingName.length; i++) {
      revyoosHoldingName[i].style.color = newColor;
    }
    var revyoosUpBtn = iframe.contentWindow.document.getElementsByClassName("revyoos-up-btn");
    for (i = 0; i < revyoosUpBtn.length; i++) {
      revyoosUpBtn[i].querySelector("path").style.stroke = newColor;
    }
    var revyoosDownBtn = iframe.contentWindow.document.getElementsByClassName("revyoos-down-btn");
    for (i = 0; i < revyoosDownBtn.length; i++) {
      revyoosDownBtn[i].querySelector("path").style.stroke = newColor;
    }
    var revyoosCloseBtn = iframe.contentWindow.document.getElementsByClassName("revyoos-close-btn");
    for (i = 0; i < revyoosCloseBtn.length; i++) {
      revyoosCloseBtn[i].querySelector("path").style.fill = newColor;
    }
  }
}
